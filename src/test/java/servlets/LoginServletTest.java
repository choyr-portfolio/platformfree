package servlets;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;

public class LoginServletTest {

	private final LoginServlet servlet = new LoginServlet();
	private final HttpServletRequest req = mock(HttpServletRequest.class);
	private final HttpServletResponse res = mock(HttpServletResponse.class);
	private final RequestDispatcher dis = mock(RequestDispatcher.class);
	private final ServletContext sc = mock(ServletContext.class);
	private final HttpSession session = mock(HttpSession.class);
	
	@Test
	public void test_login_forwards_to_login_jsp_if_not_logged_in() {
		String sUsername = "";
		try {
			when(req.getSession()).thenReturn(session);
			when(session.getAttribute("username")).thenReturn(sUsername);
			when(req.getRequestDispatcher("login.jsp")).thenReturn(dis);
			
			servlet.doGet(req, res);
			verify(dis).forward(req, res);
			
		} catch (ServletException | IOException e) {
			// do nothing
		}
	}

	@Test
	public void test_login_forwards_to_home_jsp_if_already_logged_in() {
		String sUsername = "user";
		try {
			when(req.getSession()).thenReturn(session);
			when(session.getAttribute("usern")).thenReturn(sUsername);
			when(req.getRequestDispatcher("home.jsp")).thenReturn(dis);
			
			servlet.doGet(req, res);
			verify(dis).forward(req, res);
		} catch (ServletException | IOException e) {
			// do nothing
		}
	}

//	@Test
//	public void test_login_fails_if_another_user_previously_logged_in() {
//		when(session.getAttribute("loggedIn")).thenReturn(true);
//
//	}

}
