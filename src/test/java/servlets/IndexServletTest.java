package servlets;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.*;

public class IndexServletTest {

	IndexServlet servlet;
	private HttpServletRequest req;
	private HttpServletResponse res;
	private RequestDispatcher dis;
	private HttpSession session;

	@Before
	public void setup() {
		this.servlet = new IndexServlet();
		this.req = mock(HttpServletRequest.class);
		this.res = mock(HttpServletResponse.class);
		this.dis = mock(RequestDispatcher.class);
		this.session = mock(HttpSession.class);
	}

	@Test
	public void test_do_get_sets_session_username_to_empty_string() {
		try {
			when(req.getSession()).thenReturn(session);
			when(req.getRequestDispatcher("index.jsp")).thenReturn(dis);
			servlet.doGet(req, res);
			verify(session).setAttribute("username", "");
			verify(dis).forward(req, res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
