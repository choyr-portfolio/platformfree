package servlets;

import static org.mockito.Mockito.*;

import java.io.IOException;

import static org.junit.Assert.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.*;


public class LoginUserServletTest {
	
	private final LoginUserServlet servlet = new LoginUserServlet();
	private final HttpServletRequest req = mock(HttpServletRequest.class);
	private final HttpServletResponse res = mock(HttpServletResponse.class);
	private final RequestDispatcher dis = mock(RequestDispatcher.class);
	private final ServletContext sc = mock(ServletContext.class);
	private final HttpSession session = mock(HttpSession.class);

	@Test
	public void test_username_provided_is_null_forwards_to_login_jsp() throws ServletException, IOException {
		// Arrange
		String sUsername = "";
		String reqUsername = null;
		
		when(req.getSession()).thenReturn(session);
		when(session.getAttribute("username")).thenReturn(sUsername);
		when(req.getParameter("username")).thenReturn(reqUsername);
		when(req.getRequestDispatcher("login.jsp")).thenReturn(dis);
		
		//Act
		servlet.doPost(req, res);
		
		//Assert
		verify(dis).forward(req, res);
		
	}
	
	@Test
	public void test_when_no_username_provided_forwards_to_login_jsp() throws ServletException, IOException {
		// Arrange
		String sUsername = "";
		String reqUsername = "";
		
		when(req.getSession()).thenReturn(session);
		when(session.getAttribute("username")).thenReturn(sUsername);
		when(req.getParameter("username")).thenReturn(reqUsername);
		when(req.getRequestDispatcher("login.jsp")).thenReturn(dis);
		
		//Act
		servlet.doPost(req, res);
		
		//Assert
		verify(dis).forward(req, res);
		
	}
	
	@Test
	public void test_user_is_already_logged_in_and_forwards_to_home_jsp() throws ServletException, IOException {
		// Arrange
		String sUsername = "user";
		String reqUsername = "user";
		
		when(req.getSession()).thenReturn(session);
		when(session.getAttribute("username")).thenReturn(sUsername);
		when(req.getParameter("username")).thenReturn(reqUsername);
		when(req.getRequestDispatcher("home.jsp")).thenReturn(dis);
		
		// Act
		servlet.doPost(req, res);
		
		// Assert
		verify(dis).forward(req, res);
		
	}
	
	@Test
	public void test_no_logged_in_usern_and_logging_in_with_new_username_and_forwards_to_home_jsp() throws ServletException, IOException {
		// Arrange
		String sUsername = "";
		String reqUsername = "user";
		
		when(req.getSession()).thenReturn(session);
		when(session.getAttribute("username")).thenReturn(sUsername);
		when(req.getParameter("username")).thenReturn(reqUsername);
		when(req.getRequestDispatcher("home.jsp")).thenReturn(dis);
		
		// Act
		servlet.doPost(req, res);
		
		// Assert
		verify(session).setAttribute("username", reqUsername);
		verify(dis).forward(req, res);
	}
	
}
