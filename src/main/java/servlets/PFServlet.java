package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public abstract class PFServlet extends HttpServlet {
	protected Logger logger;
	
	protected String getUsername(HttpSession session) {
		return (String) session.getAttribute("usern"); 
	}
	
	protected void setUsername(HttpSession session, String username) {
		session.setAttribute("username", username);
	}
	
	protected boolean getLoggedIn(HttpSession session) {
		return (boolean) session.getAttribute("loggedIn");
	}
	
	protected void setLoggedIn(HttpSession session, boolean bool) {
		session.setAttribute("loggedIn", bool);
	}

	
}
