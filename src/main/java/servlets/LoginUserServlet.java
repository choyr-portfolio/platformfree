package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class LoginUserServlet extends PFServlet {

	final Logger logger = Logger.getLogger(LoginUserServlet.class);

	private static final long serialVersionUID = -8106838905771552219L;
	HttpSession session;
	private String sUsername;
	private String reqUsername;
	private String reqPassword;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher = null;
		session = req.getSession();

		sUsername = (String) session.getAttribute("username");
		reqUsername = (String) req.getParameter("username");

		if (sUsername == "") { // not previously logged in
			// add new user as session user
			if (reqUsername != null && reqUsername != "") {
				session.setAttribute("username", reqUsername);
				logger.info("Logging in - Redirecting to home.jsp");
				dispatcher = req.getRequestDispatcher("home.jsp");

			} else { // if username not provided from login.jsp
				logger.info("Username not provided - Redirecting to login.jsp");
				dispatcher = req.getRequestDispatcher("login.jsp");
			}
		} else { // already logged in
			logger.info("Already Logged in - Redirecting to home.jsp");
			dispatcher = req.getRequestDispatcher("home.jsp");
		}

		sUsername = (String) session.getAttribute("username");
		if (sUsername.equals(reqUsername)) {
			logger.info("YES - session: " + sUsername + ", req: " + reqUsername);
		} else {
			logger.info("NO - session: " + sUsername + ", req: " + reqUsername);
		}
		
		dispatcher.forward(req, resp);
	}
}
