package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;


public class IndexServlet extends PFServlet {

	final static Logger logger = Logger.getLogger(IndexServlet.class);
	
	private static final long serialVersionUID = 5956240409652448238L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		
		// no username by default
		session.setAttribute("username", "");
		logger.info("Reset Session Username - Logged User Out");
		
		// show index.jsp
		RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
		dispatcher.forward(req, resp);
	}
}
