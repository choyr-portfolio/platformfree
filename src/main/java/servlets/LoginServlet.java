package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class LoginServlet extends PFServlet {

	final Logger logger = Logger.getLogger(LoginServlet.class);

	private static final long serialVersionUID = 5553158413548897221L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher = null;
		HttpSession session = req.getSession();

		// Check if there is a Username attached to the Session
		String sUsername = (String) session.getAttribute("username");
		logger.info("Current session user: " + sUsername);

		if (sUsername == "") { 
			// No Username in Session
			logger.info("Log in user - Redirecting to login.jsp");
			dispatcher = req.getRequestDispatcher("login.jsp");
			
		} else { 
			// Username exists in Session
			logger.info("Already Logged in - Redirecting to home.jsp");
			dispatcher = req.getRequestDispatcher("home.jsp");
		}

		dispatcher.forward(req, resp);

	}
}
