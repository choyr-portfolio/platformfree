<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<title>PlatformFree - OS Independent Repository for Applications</title>
</head>
<body>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
		crossorigin="anonymous"></script>

	<div class="container-fluid" id="header">
		<div class="row">
			<div class="col align-self-start">
				<!-- title of app -->
			</div>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
		</div>

	</div>
	<div class="container-fluid" id="main">
		<div class="row">
			<div class="col" id="left-panel">
				<p>Left Panel</p>
			</div>
			
			<div class="col-6" id="mid-panel">
				<h1>
					
					<a href="${pageContext.request.contextPath}/login">Login Page</a>
				</h1>
				<h2><a href="${pageContext.request.contextPath}">Cover Page</a></h2>
			</div>
			
			<div class="col" id="right-panel">
				<form action="${pageContext.request.contextPath}/login" method="post">
					<h3>Please Log in to see your profile.</h3>
					<p>
						User Name: <input type="text" name="usern" />
					</p>
					<p>
						Password: <input type="password" name="passw" />
					</p>
					<p>
						<input type="submit" value="Submit" name="loginSubmitBtn" />
					</p>
				</form>
				<c:if test="${username != '' }">
					<h2>Welcome <% request.getSession().getAttribute("username"); %></h2>
				</c:if>
			</div>
		</div>
	</div>
	<div class="container-fluid" id="footer">
		<div class="row">
			<div class="col align-self-end">
				<p>footer</p>
			</div>
		</div>
	</div>
</body>
</html>